# Damier

2D Tile-based c++/cuda engine for massive simulations.
Use OpenGL for rendering

Compile:
nvcc opglCuda.cu -o app -lglut -lGLU -lGL -w