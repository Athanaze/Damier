#include <iostream>
#include <unistd.h>
#include <GL/glut.h>
#include <stdio.h>
#include "data.h"
#include "Damier.h"


void simulation(){
    int randomInt;
    for(int x = 0; x<N; x++){
        for(int y  = 0; y < N; y++){
            randomInt = rand()%5;
            switch (randomInt) {
                case 0:
                    if(Damier::isTileEmpty(x+1, y)){
                        Damier::moveTile(x,y,x+1,y, Damier::grid[x][y]);
                        break;
                    }

                case 1:
                    if(Damier::isTileEmpty(x-1, y)){
                        Damier::moveTile(x,y,x-1,y, Damier::grid[x][y]);
                        break;
                    }

                case 2:
                    if(Damier::isTileEmpty(x, y-1)){
                        Damier::moveTile(x,y,x,y-1, Damier::grid[x][y]);
                        break;
                    }

                case 3:
                    if(Damier::isTileEmpty(x,y+1)){
                        Damier::moveTile(x,y,x,y+1, Damier::grid[x][y]);
                    }
                    break;
            }
        }
    }
}

int main(int argc, char** argv)
{
    Damier::initTiles();
    Damier::setupWindow(argc, argv);

    int c = 0;
    while(c < N_TICKS){
        c++;
        Damier::render();
        simulation();
        //usleep(1000000);
        usleep(10000);
    }
    std::cout << "c == N_TICKS" << '\n';

    return 0;
}
