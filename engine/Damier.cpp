#include <unistd.h>
#include <GL/glut.h>
#include <stdio.h>
#include "data.h"
#include "Damier.h"
int Damier::grid[1000][1000];
void Damier::setupWindow(int argc, char** argv){
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_SINGLE);
    glutInitWindowSize(WINDOW_SIZE, WINDOW_SIZE);
    glutInitWindowPosition(1920, 1080);
    glutCreateWindow("Damier");

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(0.0, 1.0, 0.0, 1.0);
    glClear(GL_COLOR_BUFFER_BIT);
}

void Damier::drawTile(int ax, int ay, int color){
    if(color != EMPTY){
        float x = ax*SIZE;
        float y = ay*SIZE;
        glBegin(GL_QUADS);
        switch (color) {
            case GRAY:
                glColor3f(0.15f, 0.15f, 0.20f);
                break;
            case RED:
                glColor3f(1.0f, 0.0f, 0.0f);
                break;
            case GREEN:
                glColor3f(0.0f, 1.0f, 0.0f);
                break;
        }
        glVertex2f(0.0f + x, 0.0f + y);
        glVertex2f(0.0f + x, SIZE + y);
        glVertex2f(SIZE + x, SIZE + y);
        glVertex2f(SIZE + x, 0.0f + y);
        glEnd();
    }
}
bool Damier::placeTile(int x, int y, int state){
    bool b = false;
    if(x < T && x >= 0){
        if(y < T && y >= 0){
            if(Damier::grid[x][y] == EMPTY){
                Damier::grid[x][y] = state;
                b = true;
            }
        }
    }
    return b;
}

void Damier::setTile(int x, int y, int state){
    grid[x][y] = state;
}
void Damier::drawBackground(){

    //Actual Background
    glBegin(GL_QUADS);
    glColor3f(0.1f, 0.1f, 0.15f);
    glVertex2f(-1.0f, -1.0f);
    glVertex2f(1.0f, -1.0f);
    glVertex2f(1.0f, 1.0f);
    glVertex2f(-1.0f, 1.0f);
    glEnd();
    for(int x = 0; x < T; x++){
        for(int y = 0; y <(T + (T/2)); y++){
            if(x % 2 == 0){
                if(y % 2 == 0){
                    drawTile(x,y, GRAY);
                }
            }
            else{
                if(y % 2 != 0){
                    drawTile(x,y, GRAY);
                }
            }
            }
        }
    }
void Damier::render(){
    glClear(GL_COLOR_BUFFER_BIT);

    drawBackground();

    for(int x = 0; x < T; x++){
        for (int y = 0; y < T; y++) {
            drawTile(x,y, grid[x][y]);
        }
    }

    glFlush();
}

void Damier::moveTile(int startX, int startY, int endX, int endY, int state){
    setTile(startX, startY, EMPTY);
    setTile(endX, endY, state);
}
bool Damier::isTileEmpty(int x, int y){
    bool r = false;
    if(x >= 0 && x < T){
        if(y >= 0 && y < T){
            if(grid[x][y] == EMPTY){
                r = true;
            }
        }
    }
    return r;
}
void Damier::initTiles(){
    int randomInt;
    for(int i = 0; i < N; i++){
        for (int j = 0; j < N; j++) {
            randomInt = rand()%4;
            switch (randomInt) {
                case 0:
                    break;
                case 1:
                    setTile(i, j, RED);
                    break;
                case 2:
                    setTile(i, j, GREEN);
                    break;
            }

        }
    }
}
