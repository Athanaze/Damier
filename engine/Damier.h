class Damier{
public:
    static const int T = 1000;
    static int grid[1000][1000];
    static void drawTile(int ax, int ay, int color);
    static bool placeTile(int x, int y, int state);
    static void setTile(int x, int y, int state);
    static void render();
    static void moveTile(int startX, int startY, int endX, int endY, int state);
    static bool isTileEmpty(int x, int y);
    static void initTiles();
    static void setupWindow(int argc, char** argv);
private:
    static void drawBackground();
};
